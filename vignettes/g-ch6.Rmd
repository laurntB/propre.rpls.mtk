---
title: "g- Chapitre 6"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{g- Chapitre 6}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r knitr config, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```

```{r propre.rpls.mtk__setup}
library(propre.rpls.mtk)
prplsmtkOptions(datadir='../data')
```


```{r setup}
ggplot2::theme_set(gouvdown::theme_gouv(plot_title_size = 14, subtitle_size  = 12, base_size = 10, caption_size = 10) +
                     ggplot2::theme(plot.caption.position =  "plot"))

indicateurs_rpls <- corse


```

# creer_verbatim_6
La fonction `creer_verbatim_6()` produit l'intertitre et les commentaires automatiques du chapitre 2. Le résultat est une liste nommée comprenant deux items. On accède au texte produit par la fonction en utilisant leur nom ("intertitre" et "commentaires"). 

```{r verbatim6, eval=FALSE}
creer_verbatim_6(indicateurs_rpls, annee = 2019)[["intertitre"]]
creer_verbatim_6(indicateurs_rpls, annee = 2019)[["commentaires"]]
```

Exemple d'utilisation ensuite : 

## `r creer_verbatim_6(indicateurs_rpls, annee = 2019)[["intertitre"]]`

`r creer_verbatim_6(indicateurs_rpls, annee = 2019)[["commentaires"]]`



# creer_graphe_6_1

La fonction `creer_graphe_6_1()` produit le graphique en bâtons représentant la répartition des logements sociaux mis en service selon leur mode de financement (avec les 5 modalités de la publication nationale : _PLUS_, _PLAI_, _PLS_, _PLI_ et _Autres financements_).

Compléter le paramètre `titre` si vous souhaitez modifier le titre par défaut. Pour insérer un saut de ligne à votre titre, ajouter la chaîne de caractère `\n` à l'endroit du saut souhaité. 

Compléter le paramètre `note_de_lecture` si vous souhaitez en ajouter une avant les sources. 

Pour le titre et la note de lecture attention aux accents et à l'encodage.

```{r graphe61}

creer_graphe_6_1(indicateurs_rpls_ref, annee = 2019)

```

# creer_carte_6_1

La fonction `creer_carte_6_1()` produit la carte représentant les loyers moyens au m2 par EPCI pour l'année N choisie.
Les bornes peuvent être choisies manuellement par l'utilisateur ( _bornes_ ) ou définies par une méthode de discrétisation ( _method_ ). Les méthodes disponibles sont celles proposées par `cartography::getBreaks()` : "sd", "equal", "quantile", "fisher-jenks", "q6", "geom", "arith", "em" or "msd". Voir la documentation du package `{[cartography](https://cran.r-project.org/web/packages/cartography/index.html)}`pour plus de détails.  
L'argument _inverse_ permet d'inverser la progression de la couleur (des couleurs chaudes vers froides ou inversement).

Compléter le paramètre `titre` si vous souhaitez modifier le titre par défaut. Pour insérer un saut de ligne à votre titre, ajouter la chaîne de caractère `\n` à l'endroit du saut souhaité. 

Compléter le paramètre `note_de_lecture` si vous souhaitez en ajouter une avant les sources. 

Pour le titre et la note de lecture attention aux accents et à l'encodage.

```{r carte61, message=FALSE, warning=FALSE}
fdcarte <- extraire_fondcarto(
  source = system.file('data-raw','countries_voisins-10m.gpkg',
                       package='propre.rpls.mtk'),
  nom_region="Corse"
)
creer_carte_6_1(indicateurs_rpls, annee = 2019, carto = fdcarte)
```

# creer_graphe_6_2

La fonction `creer_graphe_6_2()` produit le diagramme en bâtons représentant le loyer moyen selon le mode de financement, pour le parc total et le parc récent.

Compléter le paramètre `titre` si vous souhaitez modifier le titre par défaut. Pour insérer un saut de ligne à votre titre, ajouter la chaîne de caractère `\n` à l'endroit du saut souhaité. 

Compléter le paramètre `note_de_lecture` si vous souhaitez en ajouter une avant les sources. 

Pour le titre et la note de lecture attention aux accents et à l'encodage.

```{r graphe62}
creer_graphe_6_2(indicateurs_rpls, annee = 2019)
```


