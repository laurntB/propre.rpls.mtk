test_that("creer_verbatim_2() fonctionne", {

  indicateurs_rpls_ref <- lire_rpls_exemple() %>%
    dplyr::filter(Zone_ref)

  testthat::expect_is(creer_verbatim_2(indicateurs_rpls_ref, annee = 2019), "list")
  testthat::expect_is(creer_verbatim_2(indicateurs_rpls_ref, annee = 2019)[[1]], "character")
  testthat::expect_is(creer_verbatim_2(indicateurs_rpls_ref, annee = 2019)[[2]], "character")
})
