test_that("creer_graphe_1_1 fonctionne", {
  indicateurs_rpls_illustrations <- lire_rpls_exemple() %>%
    dplyr::filter(Zone_ref)

  # Test que le graphe est un ggiraph
  objet <- creer_graphe_1_1(
    indicateurs_rpls = indicateurs_rpls_illustrations, 
    annee = 2019
  )
  
  testthat::expect_equal(attr(objet, "class"), c("girafe", "htmlwidget", "ggiraph" ))

})
