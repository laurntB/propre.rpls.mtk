test_that("creer_verbatim_1() fonctionne", {

  indic_rpls_ref <- lire_rpls_exemple() %>%
   dplyr::filter(Zone_ref)

  testthat::expect_is(creer_verbatim_1(indic_rpls_ref, annee = 2019), "character")

})
