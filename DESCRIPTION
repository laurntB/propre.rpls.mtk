Package: propre.rpls.mtk
Title: Publications sur le Repertoire du Parc Locatif Social Selon la
    Methode Propre
Version: 0.6.1-4
Authors@R: 
    c(person(given = "Clément",
             family = "Belliard",
             role = "aut",
             email = "clement.belliard@developpement-durable.gouv.fr"),
      person(given = "Juliette",
             family = "Engelaere-Lefebvre",
             role = "aut",
             email = "juliette.engelaere@developpement-durable.gouv.fr"),
      person(given = "Franck",
             family = "Gaspard",
             role = "aut",
             email = "franck.gaspard@developpement-durable.gouv.fr"),
      person(given = "Daniel",
             family = "Kalioudjoglou",
             role = "aut",
             email = "daniel.kalioudjoglou@developpement-durable.gouv.fr"),
      person(given = "Murielle",
             family = "Lethrosne",
             role = c("aut", "cre"),
             email = "murielle.lethrosne@developpement-durable.gouv.fr"),
      person(given = "Arnaud",
             family = "Wilczynski",
             role = "aut",
             email = "arnaud.wilczynski@developpement-durable.gouv.fr"),
      person(given = "Laurent",
             family = "Beltran",
             role = "aut",
             email = "laurent.beltran@developpement-durable.gouv.fr"))
Description: propre.rpls.mtk permet de creer un rapport automatique sur le
    repertoire du parc locatif social pour toutes les regions de France
    metropolitaine et les DROM (c'est une version dérivée de propre.rpls.
License: GPL (>=3) + file LICENSE
Depends: 
    R (>= 3.5.0)
Imports: 
    utils,
    magrittr,
    dplyr,
    forcats,
    purrr,
    tibble,
    tidyr,
    tidyselect,
    rlang,
    lubridate,
    glue,
    stringr,
    attempt,
    scales,
    grid,
    ggplot2,
    ggiraph,
    kableExtra,
    sf,
    mapsf,
    ggspatial,
    santoku,
    patchwork,
    COGiter (>= 0.0.9),
    gouvdown,
    propre.datareg    
Suggests: 
    knitr,
    testthat (>= 2.1.0),
    stringi
VignetteBuilder: 
    knitr
Remotes: 
    maeltheuliere/COGiter,
    spyrales/gouvdown,
    git::https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/propre.datareg.git@master,
Encoding: UTF-8
LazyData: true
LazyDataCompression: bzip2
Roxygen: list(markdown = TRUE)
RoxygenNote: 7.2.3
Collate: 
    'options.R'
    'datacheck.R'
    'charger_donnees.R'
    'arrange_zonage.R'
    'filtres_communs.R'
    'calculer_indicateurs_0.R'
    'calculer_indicateurs_2_1.R'
    'calculer_indicateurs_epci.R'
    'caption.R'
    'helper_formatter.R'
    'discretisation.R'
    'creer_carte.R'
    'creer_carte_1_1.R'
    'creer_carte_5_1.R'
    'creer_carte_5_2.R'
    'creer_carte_6_1.R'
    'creer_graphe_1_1.R'
    'creer_graphe_2_1.R'
    'creer_graphe_3_1.R'
    'creer_graphe_3_2.R'
    'creer_graphe_4_1.R'
    'creer_graphe_6_1.R'
    'creer_graphe_6_2.R'
    'creer_tableau_1_1.R'
    'creer_tableau_3_1.R'
    'creer_tableau_4_1.R'
    'creer_tableau_5_1.R'
    'creer_verbatim_1.R'
    'creer_verbatim_2.R'
    'creer_verbatim_3.R'
    'creer_verbatim_4.R'
    'creer_verbatim_5.R'
    'creer_verbatim_6.R'
    'creer_verbatim_mentions_legales.R'
    'data.R'
    'dataprep.R'
    'exploiter_indicateurs.R'
    'extlbn_creer_graphe_1_2.R'
    'extlbn_creer_graphe_2_2.R'
    'extlbn_creer_graphe_4_2.R'
    'extlbn_creer_tableau_1_3.R'
    'select_epci.R'
    'extlbn_creer_tableau_3_0.R'
    'extlbn_creer_tableau_5_2.R'
    'extlbn_creer_tableau_5_3.R'
    'extlbn_creer_tableau_6_1.R'
    'fond_carto.R'
    'get_dataprep.R'
    'get_fond_carto.R'
    'get_id_reg.R'
    'parametres_entrees.R'
    'get_results.R'
    'lire_rpls_exemple.R'
    'selectionner_donnees_epci.R'
    'utils-pipe.R'
