#' Selection des epci mis en avant dans la publication
#'
#' @encoding UTF-8
#' 
#' @description Selection des EPCI pour lesquels on aura un détail
#'     dans les tableaux et graphiques de la publication.
#'
#' @param nom_reg Le nom de la region au format texte.
#' @param choix_epci Le parametre utilisateur de détail par epci au
#'     format texte : "1- Tous les EPCI de la zone" ou
#'     "2- Liste d EPCI à saisir".
#' @param epci_list Le cas echeant, le vecteur des codes epci de
#'     reference, définis dans les parametres.
#'
#' @return Un vecteur de codes EPCI.
#' @importFrom dplyr pull intersect filter
#' @importFrom tidyr unnest
#' @importFrom attempt message_if_not
#' @export
#'
#' @examples
#' select_epci(nom_reg = "Bretagne", choix_epci = "1- Tous les EPCI de la zone", epci_list = NULL)
#' select_epci(
#'   nom_reg = "Pays de la Loire", choix_epci = "2- Liste d EPCI a saisir",
#'   epci_list = c("244400404", "244400644")
#' )
select_epci <- function(nom_reg = "Bretagne", choix_epci = "1- Tous les EPCI de la zone",
                        epci_list = NULL) {
  reg <- get_id_reg(nom_reg)
  
  epci_choisis <- COGiter::liste_zone %>%
    tidyr::unnest(.data$REG) %>% 
    dplyr::filter(.data$TypeZone == "Epci", .data$REG == reg) %>%
    dplyr::pull(.data$CodeZone) %>%
    as.character()

  if (grepl("2-", choix_epci)) {
    epci_choisis <- dplyr::intersect(epci_list, epci_choisis)
    attempt::message_if_not(
      .x = length(epci_choisis) == length(epci_list), .p = isTRUE,
      msg = paste0("les EPCI saisis ne sont tous pas dans la region ", nom_reg)
    )
  }

    if (grepl("3-", choix_epci)) {
      epci_choisis <- NULL
  }
  epci_choisis
}

# to do mieux traiter les facteurs




#' Pour obtenir les epci avec un numéro d'ordre
#' 
#' @encoding UTF-8
#'
#' @param nom_region nom_region
#' @param index_order index_order
#'
#' @return data.frame des epci ordonnés
#' @export
#'
#' @importFrom dplyr filter pull 
#' 
#' @examples
#' epci_avc_ordre("Martinique")
#' epci_avc_ordre("Martinique", index_order=c(3,2,1))
#' @author LBn
epci_avc_ordre <- function(nom_region,index_order=NULL){
  
  #Datafields used 
  NOM_REG <- EPCI <- NULL
  
  epcilist <- COGiter::communes %>% 
                filter(NOM_REG == nom_region) %>% 
                pull(EPCI) %>% 
                unique()
  
  if(is.null(index_order)){
    epci_order <- seq(1,length(epcilist))
  }else{
    epci_order <- index_order
  }
  data.frame(epci_code = epcilist, epci_order = epci_order)
}
