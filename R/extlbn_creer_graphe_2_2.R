


#' Description
#'
#' Détails
#'
#' @encoding UTF-8
#' 
#' @title extlbn_creer_graphe_2_2
#' @param indicateurs_rpls indicateurs_rpls
#' @param annee annee
#' @param palette palette
#' @return graphe
#' @author LBn
#' @export
#' @include filtres_communs.R
#' @importFrom dplyr select filter mutate group_by
#' @importFrom tidyr pivot_longer
#' @importFrom tidyselect starts_with
#' @importFrom ggplot2 ggplot aes geom_bar theme scale_x_discrete coord_flip labs
#' @importFrom glue glue
#' @importFrom gouvdown scale_fill_gouv_discrete
#' @importFrom ggiraph ggiraph
extlbn_creer_graphe_2_2 <- function(indicateurs_rpls, annee, palette="pal_gouv_qual2")
{
  
  nom_region <- region_cible(indicateurs_rpls)
  
  #Datafields used 
  TypeZone <- Zone <- millesime <- type <- effectif <- total_millesime <- 
    percentages <- position_dodge2 <- NULL
  
  
  
  data_2_1 <- indicateurs_rpls %>%
    # filtre sur la région et pour le millesime souhaite
    dplyr::filter(TypeZone == "R\u00e9gions" & Zone == nom_region) %>%
    dplyr::mutate(millesime = as.numeric(as.character(millesime))) %>%
    dplyr::filter(millesime > annee - 3) %>%
    # selection des variables necessaires au graphe
    dplyr::select(millesime, dplyr::starts_with("nb_piece")) %>%
    # passage au format long de la table en distinguant le nb de piece du type de logement (recent ou pas)
    tidyr::pivot_longer(
      cols = starts_with("nb"), names_to = "type", values_to = "effectif"
    ) %>%
    # dplyr::mutate( type = gsub("_recent","", type)) %>%
    dplyr::filter( !grepl("_recent", type) ) %>%
    dplyr::group_by(millesime) %>%
    dplyr::mutate( total_millesime = sum(effectif) ) %>%
    dplyr::group_by( type, .add=TRUE ) %>%
    dplyr::mutate( percentages = effectif / total_millesime * 100) %>%
    dplyr::mutate( millesime = as.character(millesime))
  
  
  g_pie <- ggplot2::ggplot(data_2_1,
           ggplot2::aes(
            fill=millesime,
            y=percentages,
            x=type)) + 
    ggplot2::geom_bar(position=position_dodge2(width=0.2), stat="identity") +
    ggplot2::theme(aspect.ratio = 0.3) +
    ggplot2::scale_x_discrete(
      labels = c(
        "nb_piece_1" = "1 pi\u00e8ce",
        "nb_piece_2" = "2 pi\u00e8ces",
        "nb_piece_3" = "3 pi\u00e8ces",
        "nb_piece_4" = "4 pi\u00e8ces",
        "nb_piece_5_plus" = "5 ou plus")) +
    ggplot2::coord_flip() +
    ggplot2::labs(title = glue::glue("\u00C9volution du parc social "),
                  subtitle = "selon le nombre de pi\u00e8ces des logements",
                  x = "Nombre de pi\u00e8ces",
                  y = "en pourcentage du parc",
                  caption = caption(sources = 1)) + 
      gouvdown::scale_fill_gouv_discrete(palette = palette ) 
  
  ggiraph::ggiraph(code = print(g_pie))
}
