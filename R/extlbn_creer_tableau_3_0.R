




#' @importFrom dplyr inner_join
g30_indicateurs_rpls_communaux <- function(data, communes){
  rpls_details_communaux <- data %>%
    inner_join(
      communes, 
      by = c("CodeZone" = "DEPCOM"), 
      suffix = c("", ".cogiter"))
}









prepdonnees_tableau_3_0 <- function(indicateurs_rpls,annee,epci_order=NULL)
{

  #Datafields used 
  epci_order <- millesime <- NOM_EPCI <- Zone <- nb_mes <- NULL  

    nom_region <- region_cible(indicateurs_rpls)     
    communes <- COGiter::communes %>%
	    inner_join(epci_avc_ordre(nom_region,epci_order), by = c("EPCI" = "epci_code"))
    indicateurs_rpls_communaux <- g30_indicateurs_rpls_communaux(indicateurs_rpls, communes)

    indicateurs_rpls_communaux %>%
        arrange(epci_order) %>%
        filter(millesime == annee) %>%
        select(NOM_EPCI, Zone, nb_mes, millesime) %>%
        filter(nb_mes>0) %>%
        mutate(pct = nb_mes * 100 / sum(nb_mes))

}











#' Description
#'
#' Détails
#' 
#' @encoding UTF-8
#' @title extlbn_creer_tableau_3_0
#' @param indicateurs_rpls indicateurs_rpls
#' @param annee annee
#' @param epci_order p_order_ref
#' @return kable
#' @author LBn
#' @export
#' @include select_epci.R
#' @importFrom dplyr inner_join arrange filter select mutate
#' @importFrom kableExtra kable collapse_rows kable_styling row_spec footnote
extlbn_creer_tableau_3_0 <- function(indicateurs_rpls,annee,epci_order=NULL)
{

    data <- prepdonnees_tableau_3_0(indicateurs_rpls,annee,epci_order=NULL)

    gntd_caption <- mktbl_caption(
        paste0("Le parc locatif social ",
                                       " au 1er janvier ", annee),
        "t30"
    )
   
    #Datafields used 
    millesime <- NULL  
    
    data %>%
        dplyr::select(-millesime) %>%
        knitr::kable(
            "html",
            col.names=c("EPCI",
                        "Commune",
                        "Nombre de logement",
                        "En pourcentage des mises en service"),
            escape = FALSE,
            digits = c(0,0,1,1),
            format.args = list(big.mark = " ",
                               decimal.mark = ","),
            caption = gntd_caption,
            label = NA,
            booktabs = TRUE)%>%
        kableExtra::collapse_rows(columns=1) %>%
        kableExtra::kable_styling(font_size = 12) %>%
                                        # Formatage des lignes "EPCI" : fond gris foncé, gras
        ## kableExtra::row_spec(which(dplyr::pull(data, .data$CodeZone) == ""), 
        ##                      bold = T, 
        ##                      background = "#C4C4C4")  %>%    
                                        # Création note de bas de page
        kableExtra::footnote(general = caption(sources = 1), general_title = "")

}
