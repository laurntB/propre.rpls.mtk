



#' genfunc_indicateurs_epci
#' 
#' ... en cours d'écriture ...
#' 
#' @encoding UTF-8
#'
#' @param indicateurs_rpls  param
#' @param annee      param
#'
#' @importFrom dplyr filter mutate
#' 
#' 
#' @return valeur retournée est une fonction qui accepte 
#'         2 paramètres cogepci,var où var est un identifiant 
#'         pris dans la liste des indicateurs connus augmentés de
#'         ls_pct_in_reg et pop_pct_in_reg
#'         
#' @export
#' @include filtres_communs.R
#'
#' @examples
#' genfunc_indicateurs_epci(martinique,2020)
genfunc_indicateurs_epci <- function(indicateurs_rpls, annee)
{
  nom_region <- region_cible(indicateurs_rpls)
    
  #Datafields used 
  nb_logt_total <- CodeZone <- NULL
  
  epci_annee_n <- selectionner_donnees_epci(indicateurs_rpls, nom_region, annee)
    
  ##> unique( epci_annee_n %>% select(Zone,CodeZone) )
  ## # A tibble: 3 x 2
  ##   Zone                                CodeZone
  ##   <fct>                               <fct>
  ## 1 CA de l'Espace Sud de la Martinique 249720053
  ## 2 CA du Centre de la Martinique       249720061
  ## 3 CA du Pays Nord Martinique          200041788
  ## >
    
  data <- epci_annee_n %>%
    mutate(
      ls_pct_in_reg = nb_logt_total / sum(nb_logt_total) * 100,
      pop_pct_in_reg = -999
    ) 
  

  gi<-function(cogepci,var){
    vv <- data %>%
      filter(CodeZone==cogepci) %>%
      pull(!!var)
    vv[1]
  }
  
  ## gi('249720053','pop_pct')
  
  gi
}
