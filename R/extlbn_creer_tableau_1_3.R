





#' Données à l'échelle des communes 
#' 
#' @encoding UTF-8
#'
#' @param indicateurs_rpls indicateurs_rpls
#' @param annee annee
#' @param with_order with_order
#'
#' @return dataframe
#' 
#' @importFrom dplyr filter inner_join select mutate left_join ungroup group_by all_of
#' @importFrom tidyr pivot_wider
#' 
#' @export
#'
construire_donnees_communes <- function(indicateurs_rpls, annee, with_order=NULL){
    
    ## Datafields used 
    millesime <- evolution_n_nmoins5 <- densite_ls_rp <- 
      nb_logt_total <- CodeZone <- nb_logt_total_nm1 <- NOM_DEPCOM <- 
      prct <- evo_nm1_nb <- nb_logt_total_nm5 <- EPCI <- NOM_EPCI <- 
      regcible <- epci_order <- CodeZone <- NOM_DEPCOM<- 
      nb_logt_total_nm0 <- prct_nm0 <- evo_nm1_nb <- 
      nb_logt_total_nm5 <- evolution_n_nmoins5_nm0<- 
      densite_ls_rp_nm0nb_logt_total_nm0 <- nb_logt_total_nm1 <- TypeZone <- 
      Zone <- year_relative <- densite_ls_rp_nm0 <- evolution_n_nmoins1 <- 
      NULL
    
    nom_region <- region_cible(indicateurs_rpls)     

    colssel <- c("millesime", "CodeZone", "Zone", "nb_logt_total", "evolution_n_nmoins1", 
                 "evolution_n_nmoins5", "densite_ls_rp")
    tmpdata <- indicateurs_rpls %>% 
        filter(
            TypeZone == 'Communes',
            millesime == annee|millesime == annee-1|millesime == annee-5) %>% 
        select(all_of(colssel)) %>%
        group_by(millesime) %>% 
        mutate(prct = nb_logt_total * 100 / sum(nb_logt_total)) %>%
        ungroup() %>% 
        mutate(
            year_relative = annee - as.numeric(as.character(millesime)),
            EPCI = COGiter::code_epci_of_com(CodeZone)) %>%
        select(-millesime) %>% 
        pivot_wider(id_cols=c(CodeZone, Zone,EPCI),
                    names_from = year_relative,
                    names_glue = "{.value}_nm{year_relative}",
                    values_from = c(nb_logt_total,prct,evolution_n_nmoins1,
                                    evolution_n_nmoins5,densite_ls_rp))
    
    epci_ordonnes <- epci_avc_ordre(nom_region,with_order)
    
    data_communes <- tmpdata %>% 
        inner_join(epci_ordonnes, by=c('EPCI'='epci_code')) %>% 
        mutate(regcible = nom_region,
               NOM_DEPCOM = Zone,
               evo_nm1_nb = nb_logt_total_nm0 - nb_logt_total_nm1) %>%
        select(regcible, CodeZone, NOM_DEPCOM, nb_logt_total_nm0, prct_nm0, evo_nm1_nb, 
               nb_logt_total_nm5, evolution_n_nmoins5_nm0, densite_ls_rp_nm0, EPCI, epci_order) %>% 
        arrange(epci_order)
    
    data_communes
}







#' Données à l'échelle epci
#' 
#' @encoding UTF-8
#'
#' @param indicateurs_rpls indicateurs_rpls
#' @param annee annee
#' @param with_order index_order
#'
#' @return un tableau de données
#' 
#' @importFrom dplyr filter inner_join select mutate left_join all_of group_by arrange
#' @importFrom tidyr pivot_wider
#' 
#' @export
#'
#' @examples
#' \dontrun{
#' params <- list(
#'   nom_region   = "02 Martinique", 
#'   epci_ref     = "", 
#'   epci_list    = c("249720053","249720061","200041788"),
#'   annee        = "2021",
#'   rebuild_data = FALSE
#' )
#' zonage_sep <- loaddata_zonage_spe()
#' indicateurs_rpls <- get_dataprep(par_util = params, maj = rebuild_data) %>%
#'   dplyr::filter(Zone_ref) %>% arrange_zonage(zonage_spe)
#' construire_donnees_epci(indicateurs_rpls, 2020)
#' }
construire_donnees_epci  <-  function(indicateurs_rpls, annee, with_order=NULL){
    
    
    ## Datafields used 
    CodeZone <- epci_code <- millesime <- 
    evolution_n_nmoins5 <- densite_ls_rp <- nb_logt_total <- NOM_EPCI <- 
    NOM_DEPCOM <- prct <- evo_nm1_nb <- nb_logt_total_nm5 <- EPCI <- 
    regcible <- epci_order <- regcible<- CodeZone<- NOM_DEPCOM<- 
    nb_logt_total_nm0<- prct_nm0<- evo_nm1_nb<- 
    nb_logt_total_nm5<- evolution_n_nmoins5_nm0<- densite_ls_rp_nm0 <- EPCI <- 
    epci_order <- TypeZone <- year_relative <- evolution_n_nmoins1 <- 
      Zone <- nb_logt_total_nm1 <- NULL  
    
    
    nom_region <- region_cible(indicateurs_rpls) 

    colssel <- c("millesime", "CodeZone", "Zone", "nb_logt_total", "evolution_n_nmoins1",
                 "evolution_n_nmoins5", "densite_ls_rp")
    tmpdata <- indicateurs_rpls %>% 
        filter(
            TypeZone == 'Epci',
            millesime == annee|millesime == annee-1|millesime == annee-5) %>% 
        select(all_of(colssel)) %>%
        group_by(millesime) %>% 
        mutate(prct = nb_logt_total * 100 / sum(nb_logt_total)) %>%
        ungroup() %>% 
        mutate(year_relative = annee - as.numeric(as.character(millesime))) %>%
        select(-millesime) %>% 
        pivot_wider(id_cols=c("CodeZone", "Zone"),
                    names_from = year_relative,
                    names_glue = "{.value}_nm{year_relative}",
                    values_from = c(nb_logt_total,prct,evolution_n_nmoins1,
                                    evolution_n_nmoins5,densite_ls_rp))
    

    epci_ordonnes <- epci_avc_ordre(nom_region,with_order)
    
    data_epci <- tmpdata %>% 
        inner_join(epci_ordonnes, by = c("CodeZone" = "epci_code")) %>% 
        mutate(
            regcible = nom_region,
            EPCI = CodeZone,
            CodeZone = "",
            NOM_DEPCOM = Zone,
            evo_nm1_nb = nb_logt_total_nm0 - nb_logt_total_nm1
        ) %>%
                                        # select(regcible, CodeZone, NOM_DEPCOM, nb_logt_total_nm0, prct_nm0, evo_nm1_nb, 
                                        #        nb_logt_total_nm5, evolution_n_nmoins5_nm0, densite_ls_rp_nm0, EPCI, epci_order) %>% 
        select(regcible, CodeZone, NOM_DEPCOM, nb_logt_total_nm0, prct_nm0, evo_nm1_nb, 
               nb_logt_total_nm5, evolution_n_nmoins5_nm0, densite_ls_rp_nm0, EPCI, epci_order) %>% 
        arrange(epci_order)
    
    data_epci
}







#' Creation tableau de détail communaux
#'
#' @description Mise en page du tableau du chapitre 1 au format html.
#'
#' @param indicateurs_rpls indicateurs_rpls
#' @param annee Le millesime renseigné par l'utilisateur.
#' @param with_order index_order=NULL
#' 
#' @return un tableau de données
#'
#' @importFrom dplyr bind_rows mutate arrange
#' 
#' @examples
#' \dontrun{
#' data <- donnees_communales(indicateurs_rpls, indicateurs_rpls_ref, 2018)
#' cree_tableau_details_communaux(data)
#' }
#' 
#' @include arrange_zonage.R
#' @export
construire_donnees_epci_et_com <- function(indicateurs_rpls, annee, with_order=NULL){
    
                                        ## Datafields used 
    epci_order <- NULL
    
                                        ## rpls_details_communaux
    data_communes <- construire_donnees_communes(indicateurs_rpls, annee, with_order)
    
                                        ## rpls_details_epci
    data_epci <- construire_donnees_epci(indicateurs_rpls, annee, with_order)
    
    data <- bind_rows(data_communes, data_epci) %>%
        arrange(epci_order) 
    
    
    data %>% 
        mutate(millesime=annee)
}




#' Description
#'
#' Détails
#' @encoding UTF-8
#' @title trouver_commune_plus_grd_nb_lgts
#' @param indicateurs_rpls indicateurs_rpls
#' @param annee annee
#' @return value
#' @author LBn
#' @importFrom dplyr select mutate filter
#' @export
trouver_commune_plus_grd_nb_lgts <- function(indicateurs_rpls, annee)
{
                                        ## Datafields used 
    NOM_DEPCOM <- nb_logt_total <- nb_logt_total_nm0 <- NULL
    
    construire_donnees_communes(indicateurs_rpls, annee) %>% 
        dplyr::rename(nb_logt_total = nb_logt_total_nm0) %>% 
        dplyr::select(NOM_DEPCOM, nb_logt_total) %>%
        dplyr::mutate(percent = nb_logt_total / sum(nb_logt_total) * 100) %>%
        dplyr::filter(nb_logt_total == max(nb_logt_total))

}



























#' Formattage tableau de détail communaux
#' 
#' @encoding UTF-8
#'
#' @description Mise en page du tableau du chapitre 1 au format html.
#'
#' @param data La table d'indicateurs préparée par la fonction [construire_donnees_epci_et_com]
#'
#' @return Un tableau mise en page au format html.
#'
#' @importFrom dplyr select pull
#' @importFrom glue glue
#' @importFrom kableExtra kable kable_styling row_spec footnote
#' 
#' @examples
#' \dontrun{
#' cree_tableau_details_communaux(data,nom_region)
#' }
#' @export
#' @include filtres_communs.R
creer_tableau_details_communaux <- function(data){
    
                                        #Datafields used 
    regcible <- NOM_EPCI <- epci_order <- EPCI <- millesime <- CodeZone <- NULL
    
    
                                        #param nom_region nom de la région pour subtilités de formulation des titres
                                        # nom_region <- data %>% distinct(regcible) %>% pull(regcible) %>% head(n=1L)
    nom_region <- data$regcible[1]
    
                                        # creation du CC lieu pour exception la Reunion et Mayotte dans le titre du tableau
    local <- ifelse(get_id_reg(nom_region) %in% c("04","06") , 
                    paste0("de ", nom_region), 
                    paste0("en ", nom_region))
    
    annee <- data$millesime[1]
    anneem5 <- annee - 5

    gntd_caption <- mktbl_caption(
        paste0("Le parc locatif social ",
               local ,
               " au 1er janvier ", annee),
        "t13")
    
    tableau <- data %>% 
        dplyr::select( -regcible, -epci_order, -EPCI, -millesime) %>%
                                        # Mise en place des titres de colonnes
        knitr::kable(
           "html",
           col.names=c(
               "",
               "Commune",
               glue::glue("Nombre de logements sociaux au 01/01/{annee}"),
               "% sur total",
               "Evolution sur un an",
               glue::glue("Nombre de logements sociaux au 01/01/{anneem5}"),
               glue::glue("Evolution {anneem5}-{annee}(en %)"),
               "Densit\u00e9 pour 100 r\u00E9sidences principales"),
           escape = FALSE,
           digits = c(0,0,1,1,1,1,1,1),
           format.args = list(big.mark = " ",
                              decimal.mark = ","),
           caption = gntd_caption,
           label = NA,
           booktabs = TRUE) %>%
        kableExtra::kable_styling(font_size = 12) %>%
                                        # Formatage des lignes "EPCI" : fond gris foncé, gras
        kableExtra::row_spec(which(dplyr::pull(data, CodeZone) == ""), 
                             bold = T, 
                             background = "#C4C4C4")  %>%    
                                        # Création note de bas de page
        kableExtra::footnote(general = caption(sources = 2), general_title = "")

    tableau
}








#' Creation tableau de détail communaux
#'
#' @param indicateurs_rpls indicateurs_rpls
#' @param annee annee
#' @param with_order ordre d'affichage des epci
#'
#' @return un tableau KableExtra
#' 
#' @export
#'
#' @author LBn
creer_tableau_1_3 <- function(indicateurs_rpls,annee, with_order)
{
    construire_donnees_epci_et_com(indicateurs_rpls,annee, with_order) %>% 
        creer_tableau_details_communaux()
}







