#' Chapitre 6: Verbatim
#'
#' @encoding UTF-8
#'
#' 
#' @description Production des commentaires et de l'intertitre
#'     verbatim du chapitre 6.
#' @param indicateurs_rpls La table d'indicateurs préparée par
#'     dataprep() selon les inputs de l'utilisateur
#' @param annee Le millesime renseigné par l'utilisateur (au format
#'     numérique)
#'
#' @return Un vecteur de 2 chaînes de caractères comprenant
#'     l'intertitre et les commentaires essentiels du chapitre 6
#'
#' @importFrom dplyr filter select transmute pull contains mutate case_when starts_with across everything
#' @importFrom glue glue
#' @importFrom tidyr pivot_wider
#' @importFrom propre.datareg datareg
#'
#' @export
#'
#' @examples
#' creer_verbatim_6(martinique, annee = 2019)[["intertitre"]]
#' creer_verbatim_6(martinique, annee = 2019)[["commentaires"]]
creer_verbatim_6 <- function(indicateurs_rpls, annee) {

  data <- indicateurs_rpls
  
  #Datafields used 
  millesime <- TypeZone <- CodeZone <- Zone_ref <- nb_plai <- nb_mes <- 
    nb_plus <- part_plai <- part_plus <- somme_loyer <- somme_surface <- 
    somme_loyer_recent <- somme_surface_recent <- somme_loyer_nb_plai <- 
    somme_surface_nb_plai <- somme_loyer_nb_pls <- somme_surface_nb_pls <- 
    reg_comp <- loyer_moy <- rang <- rang_loy <- NULL
  
  
  # on calcule d'abord les indicateurs necessaires aux commentaires
  mises_svc <- data %>%
    # on garde les lignes pertinentes : Fce metro si reg metropolitaine (CodeZone == FRMETRO) ou Fce entiere si DROM (CodeZone == FRMETRODROM)
    dplyr::filter(millesime == annee, grepl("gion", TypeZone) | grepl("FRMETRO", CodeZone), Zone_ref) %>%
    dplyr::select(millesime, TypeZone, nb_plai, nb_mes, nb_plus) %>%
    dplyr::transmute(TypeZone = substr(TypeZone, 1, 1), # on simplifie le lib de niv geo pour limiter les soucis d'encodage
                  part_plai = round(nb_plai / nb_mes * 100, 1),
                  part_plus = round(nb_plus / nb_mes * 100, 1)) %>%
    tidyr::pivot_wider(names_from = TypeZone, values_from = c(part_plai, part_plus))

  # une variable pour recuperer l'identifiant de la région choisi
  id_reg <- dplyr::filter(data, Zone_ref, grepl("gion", TypeZone)) %>%
    dplyr::pull(CodeZone) %>% unique() %>% as.character

  # un booleen pour determiner si la region choisie est metropolitaine ou non
  metro <- !(id_reg %in% paste0("0", 1:6))

  loyers_0 <- data %>%
    dplyr::filter(millesime == annee, grepl("gions", TypeZone) | grepl("FRMETRO", CodeZone)) %>%
    dplyr::select(dplyr::contains("Zone"), somme_loyer, somme_surface, somme_loyer_recent, somme_surface_recent,
                  somme_loyer_nb_plai, somme_surface_nb_plai, somme_loyer_nb_pls, somme_surface_nb_pls) %>%
    dplyr::mutate(TypeZone = substr(TypeZone, 1, 1),
                  reg_comp = dplyr::case_when(
                    TypeZone != "R" ~ FALSE,
                    TypeZone == "R" & metro & CodeZone %in% paste0("0", 1:6) ~ FALSE,
                    TypeZone == "R" & metro & !(CodeZone %in% paste0("0", 1:6)) ~ TRUE,
                    TypeZone == "R" & !metro & CodeZone %in% paste0("0", 1:6) ~ TRUE,
                    TypeZone == "R" & !metro & !(CodeZone %in% paste0("0", 1:6)) ~ FALSE,
                    TRUE ~ FALSE),
                  loyer_moy = somme_loyer / somme_surface ,
                  loyer_moy_recent = somme_loyer_recent / somme_surface_recent ,
                  loyer_moy_plai = somme_loyer_nb_plai / somme_surface_nb_plai ,
                  loyer_moy_pls = somme_loyer_nb_pls / somme_surface_nb_pls) %>%
    dplyr::select(-dplyr::starts_with("somme_"))

  loyers_rang <- loyers_0 %>%
    dplyr::filter(reg_comp) %>%
    dplyr::select(Zone_ref, loyer_moy) %>%
    dplyr::mutate(rang = rank(-loyer_moy),
                  dernier = (rang == nrow(.)),    # hack line numbering
                  rang_loy = dplyr::case_when(
                    rang == 1 ~ "1er",
                    dernier ~ "dernier",
                    TRUE ~ paste0(rang, "e"))) %>%
    dplyr::filter(Zone_ref) %>%
    dplyr::pull(rang_loy)


  loyers <- loyers_0 %>%
    dplyr::filter(Zone_ref) %>%
    dplyr::select(TypeZone, dplyr::starts_with("loyer_moy")) %>%
    tidyr::pivot_wider(names_from = TypeZone, values_from = dplyr::starts_with("loyer_moy")) %>%
    dplyr::mutate(dplyr::across(dplyr::everything(), format_fr_nb))


  # on récupère les formulations idiomatiques grâce à {propre.datareg}
  verb_reg <- propre.datareg::datareg(code_reg = id_reg)


  # on cree ensuite une liste nommee des differents parametres
  verb6 <- list(part_mes_plai_r = mises_svc$part_plai_R[1] %>% format_fr_pct,
                part_mes_plus_r = mises_svc$part_plus_R[1] %>% format_fr_pct,
                annee_prec = annee - 1,
                dans_la_region = verb_reg$dans_la_region,
                part_mes_plai_fr = mises_svc$part_plai_F[1] %>% format_fr_pct,
                comp_fr_mes_plai = ifelse(metro, "France m\u00e9tropolitaine", "France enti\u00e8re"),
                loyer_moy_r = loyers$loyer_moy_R,
                reg_dep = verb_reg$la_region,
                rang = loyers_rang,
                reg_rang = ifelse(metro, "des r\u00e9gions les plus ch\u00e8res de m\u00e9tropole", "des d\u00e9partements les plus chers d\u2019outre mer"),
                loy_moy_recent_r = loyers$loyer_moy_recent_R,
                loy_moy_recent_fr = loyers$loyer_moy_recent_F,
                comp_fr_loy_recent = ifelse(metro, "en France m\u00e9tropolitaine", "sur l\'ensemble de la France"),
                loyer_moy_plai_r = loyers$loyer_moy_plai_R,
                loyer_moy_pls_r = loyers$loyer_moy_pls_R
                )


  # production du verbatim a partir des elements precedents
  verbatim_chap_6 <- list(intertitre ="", commentaires ="")
  verbatim_chap_6$intertitre <- glue::glue("{verb6$part_mes_plai_r} des mises en service \u00e0 destination des plus pr\u00e9caires")
  verbatim_chap_6$commentaires <- glue::glue(
  "Le PLUS (Pr\u00eat locatif \u00e0 usage social) finance {verb6$part_mes_plus_r} des logements mis en service en {verb6$annee_prec} {verb6$dans_la_region}. Le PLAI
  (pr\u00eat locatif aid\u00e9 d\u2019int\u00e9gration) finance des logements \u00e0 destination des publics les plus en difficult\u00e9s. Il a concern\u00e9
  {verb6$part_mes_plai_r} des mises en service en {verb6$annee_prec} {verb6$dans_la_region} contre {verb6$part_mes_plai_fr} en {verb6$comp_fr_mes_plai}.

  Le loyer moyen s\u2019\u00e9l\u00e8ve \u00e0 {verb6$loyer_moy_r}\u202f\u20ac/m\u00b2 en {annee}, ce qui situe {verb6$reg_dep} au {verb6$rang} rang {verb6$reg_rang}.
  Dans le parc r\u00e9cent (mis en service depuis 5 ans ou moins), le loyer moyen s\u2019\u00e9tablit \u00e0 {verb6$loy_moy_recent_r}\u202f\u20ac/m\u00b2 contre
  {verb6$loy_moy_recent_fr}\u202f\u20ac/m\u00b2 {verb6$comp_fr_loy_recent}.

  Le loyer moyen est par ailleurs conditionn\u00e9 par le mode de financement initial. Il s\u2019affiche \u00e0 {verb6$loyer_moy_plai_r}\u202f\u20ac/m\u00b2 pour
  les logements financ\u00e9s par un PLAI et {verb6$loyer_moy_pls_r}\u202f\u20ac/m\u00b2 pour ceux financ\u00e9s par un PLS."
  )

  verbatim_chap_6
}
