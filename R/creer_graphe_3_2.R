#' Creation du 2e graphique du chapitre sur les mises en service et
#' les sorties, representant les mises en services en et hors
#' quartiers prioritaires de la politique de la ville.
#'
#' @encoding UTF-8
#'
#' @description Création du graphique en barres réprésentant le nombre
#'     de logements entrés dans le patrimoine des bailleurs selon
#'     qu'ils sont situés ou non en zone QPV (quartier prioritaire de
#'     la ville)
#' @param indicateurs_rpls La table d'indicateurs préparée par
#'     dataprep() selon les inputs de l'utilisateur et filtrée sur le
#'     booléen Zone_ref.
#' @param annee Une année parmi les millésimes sélectionnables par
#'     l'utilisateur, au format numérique.
#' @param palette choix de la palette de couleur parmi celle de
#'     \code{gouvdown::\link{scale_color_gouv_discrete}}
#' @param titre une chaine de caractère si vous voulez ajouter un
#'     titre spécifique. (par défaut:
#'     "Logements mis en service en {annee-1} dans et hors quartiers prioritaires de la politique de la ville (QPV)")
#' @param note_de_lecture une chaine de caractère si vous voulez
#'     ajouter une note de lecture en dessous des sources
#'
#' @return Un graphique en barres interactif
#'
#' @importFrom dplyr filter select mutate case_when pull
#' @importFrom forcats fct_relevel fct_drop
#' @importFrom ggiraph geom_bar_interactive ggiraph
#' @importFrom ggplot2 ggplot aes labs
#' @importFrom gouvdown scale_fill_gouv_discrete
#' @importFrom stringr str_detect
#' @importFrom tidyr pivot_longer
#' @importFrom rlang .data
#' @importFrom glue glue
#'
#' @export
#'
#' @examples
#' creer_graphe_3_2(martinique, annee = 2019, note_de_lecture = "")



creer_graphe_3_2 <- function(indicateurs_rpls, annee, palette = "pal_gouv_div1",
                             titre = NULL,
                             note_de_lecture = ""){

  data <- indicateurs_rpls
  
  #Datafields used 
  TypeZone <- millesime <- Zone <- nb_mes_qpv_construit_org <- 
    nb_mes_qpv_acq_av_travaux <- nb_mes_qpv_acq_ss_travaux <- 
    nb_mes_qpv_acq_vefa <- nb_mes_nonqpv_construit_org <- 
    nb_mes_nonqpv_acq_av_travaux <- nb_mes_nonqpv_acq_ss_travaux <- 
    nb_mes_nonqpv_acq_vefa <- Variable <- Entree_patrimoine <- 
    Nb_logements <- Zone_QPV <- NULL
  
  if (is.null(titre)){
    titre <- "Logements mis en service en {annee-1} dans et hors quartiers prioritaires de la politique de la ville (QPV)"}
  # Création de la table utile à la production du graphique
  tab <- data %>%
    # Filtres pour ne garder que les données de la région et du millésime sélectionné
    dplyr::filter(TypeZone == "R\u00e9gions",
                  millesime == annee) %>%
    # Sélection des variables utiles pour le graphique
    dplyr::select(TypeZone, Zone, millesime,
                  nb_mes_qpv_construit_org, nb_mes_qpv_acq_av_travaux,
                  nb_mes_qpv_acq_ss_travaux, nb_mes_qpv_acq_vefa,
                  nb_mes_nonqpv_construit_org, nb_mes_nonqpv_acq_av_travaux,
                  nb_mes_nonqpv_acq_ss_travaux, nb_mes_nonqpv_acq_vefa) %>%
    # Passage du format large au format long
    tidyr::pivot_longer(-c(TypeZone, Zone, millesime),
                        values_to = "Nb_logements", names_to = c("Variable"),
                        names_pattern = "nb_mes_?(.*)") %>%
    # Création des deux variables relatives à QPV / hors QPV et à l'entrée dans le patrimoine
    dplyr::mutate(Zone_QPV = ifelse(stringr::str_detect(string = Variable, pattern = "^qpv"), "En QPV", "Hors QPV"),
                  Entree_patrimoine = dplyr::case_when(
                    stringr::str_detect(string = Variable, pattern = "construit_org") ~ "Construit par\nl\'organisme",
                    stringr::str_detect(string = Variable, pattern = "acq_av_travaux") ~ "Acquis avec\ntravaux",
                    stringr::str_detect(string = Variable, pattern = "acq_ss_travaux") ~ "Acquis sans\ntravaux",
                    stringr::str_detect(string = Variable, pattern = "acq_vefa") ~ "Acquis en\nVEFA"),
                  Entree_patrimoine = as.factor(Entree_patrimoine),
                  Entree_patrimoine = forcats::fct_relevel(Entree_patrimoine,
                                                           "Construit par\nl\'organisme", "Acquis en\nVEFA",
                                                           "Acquis avec\ntravaux", "Acquis sans\ntravaux"),
                  # Suppression des modalités de "millesime" non retenues
                  millesime = forcats::fct_drop(millesime))


  # Création du graphique
  graphe <- ggplot2::ggplot(data = tab, mapping = ggplot2::aes(x = Entree_patrimoine, y = Nb_logements,
                                                               fill = Zone_QPV)) +
    # Création graphique en barres en mode interactif
    ggiraph::geom_bar_interactive(stat = "identity", data_id = row.names(tab),
                                  tooltip = format(dplyr::pull(tab, Nb_logements), big.mark = " ")) +
    # Paramétrage titre, sous-titre, axes ...
    ggplot2::labs(title = stringr::str_wrap(glue::glue(titre),
                                            width = 55),
                  subtitle = "Unit\u00e9 : nombre de logements",
                  x = "",
                  y = "",
                  fill = "",
                  caption = dplyr::if_else(note_de_lecture != "" ,
                                           paste0(note_de_lecture, "\n\n", caption(sources = 1)),
                                           caption(sources = 1))) +
    ggplot2::scale_y_continuous(labels = function(x) format(x, big.mark = " "))  +
    scale_fill_gouv_discrete(palette = palette)
  # Ajout de l'interactivté
  ggiraph::ggiraph(code = print(graphe))

}
