#' fonction utilitaire de formatage en pourcentage pour le francais
#'
#' @description fonction utilitaire de formatage en pourcentage pour le francais

#' @param x un nombre à formater en pourcentage
#' @param dec un entier désignant le nombre de chiffres après la virgule souhaité (1 par défaut)
#'
#' @return une chaîne de texte, x %, avec transformation de la décimale en virgule et insertion d'un espace insécable
#'
#' @importFrom attempt stop_if_not
#'
#' @export
#'
#' @examples
#' format_fr_pct(100/3)
#'
format_fr_pct <- function(x, dec = 1) {
  attempt::stop_if_not(x, is.numeric, msg = "x n'est pas un nombre, revoyez la saisie de l'argument de format_fr_pct(x, dec)")
  paste0(formatC(x, decimal.mark = ",", big.mark = "\u202f", format = "f", digits = dec), "\u00a0%")
}





#' fonction utilitaire de formatage de nombre pour le francais
#'
#' @description fonction utilitaire de formatage de nombre pour le francais
#'
#' @param x un nombre à formater en français
#' @param dec un entier désignant le nombre de chiffres après la virgule souhaité (1 par défaut)
#' @param with_big_mark with_big_mark
#' @param unit unit
#' @param always_sign always_sign
#'
#' @return une chaîne de texte avec transformation de la décimale en virgule et insertion d'un caractère spécifié via big_mark au niveau du séparateur de milliers
#'
#' @importFrom attempt stop_if_not
#'
#' @export
#'
#' @examples
#' format_fr_nb(10000000/7)
format_fr_nb <- function(x, dec = 1, with_big_mark=TRUE, unit="", always_sign = FALSE) 
{
  
  attempt::stop_if_not(x, is.numeric, msg = "x n'est pas un nombre, revoyez la saisie de l'argument de format_fr_nb(x, dec)")

  big_mark <- ""
  if(with_big_mark) {big_mark <- " "}  ## "\u202f"
  
  this.flag <- ''
  if(always_sign){
    this.flag <- '+'
  }
  
  out <- formatC(
    x, 
    decimal.mark = ",", 
    format = "f", 
    digits = dec, 
    big.mark = big_mark,
    flag = this.flag
  )
  
  paste0(out,unit)
}


