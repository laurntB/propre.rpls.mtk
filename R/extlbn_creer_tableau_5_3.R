prepdonnees_tableau_5_3 <- function(indicateurs_rpls,annee)
{
    nom_region <- region_cible(indicateurs_rpls)
    
                                        #Datafields used 
    Zone <- millesime <- taux_mobilite <- NULL
    
    evo_tmob <- indicateurs_rpls %>%
        filter(Zone == nom_region) %>%
        select(Zone, millesime, taux_mobilite)
    
    tableau <- evo_tmob %>%
        dplyr::select(-Zone)
    
}










#' desc.
#'
#' details
#'
#' @encoding UTF-8
#' @title extlbn_creer_tableau_5_3
#' @param indicateurs_rpls indicateurs_rpls
#' @param annee annee
#' @return ggplot2
#' @author LBn
#' @export
#' @include filtres_communs.R
#' @importFrom dplyr select filter
#' @importFrom kableExtra kable kable_styling footnote
extlbn_creer_tableau_5_3 <- function(indicateurs_rpls,annee)
{
    data <- prepdonnees_tableau_5_3(indicateurs_rpls,annee)

    gntd_caption <- mktbl_caption(
        paste0("Evolution du taux de mobilit\u00E9 dans le temps"),
        "t53")
    
    data %>%
        knitr::kable(
                   "html",
                   col.names=c("Ann\u00E9e",
                               "Taux de mobilit\u00E9"),
                   escape = FALSE,
                   digits  = , c(1,1),
                   format.args = list(big.mark = " ",
                                      decimal.mark = ","),
                   caption = gntd_caption,
                   label = NA,
                   booktabs = TRUE)%>%
        kableExtra::kable_styling(font_size = 12) %>%
                                        # Formatage des lignes "EPCI" : fond gris foncé, gras
        ## kableExtra::row_spec(which(dplyr::pull(data, .data$CodeZone) == ""), 
        ##                      bold = T, 
        ##                      background = "#C4C4C4")  %>%    
                                        # Création note de bas de page
        kableExtra::footnote(general = caption(sources = 1), general_title = "")
    
}
