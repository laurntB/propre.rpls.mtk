#' Chapitre 3: Verbatim
#'
#' @encoding UTF-8
#' 
#' @description Production des commentaires verbatim du chapitre 3.
#' @param indicateurs_rpls La table d'indicateurs préparée par
#'     dataprep() selon les inputs de l'utilisateur et filtrée sur le
#'     booléen Zone_ref
#' @param annee Le millesime renseigné par l'utilisateur (au format
#'     numérique)
#'
#' @return Un vecteur de 4 chaînes de caractères comprenant
#'     l'intertitre, les commentaires, le titre et le texte de l
#'     encadre du chapitre 3
#'
#' @importFrom dplyr filter select transmute mutate case_when pull
#' @importFrom tidyr pivot_wider
#' @importFrom glue glue
#' @importFrom propre.datareg datareg maj1let
#'
#' @export
#'
#' @examples
#' verbatim3 <- creer_verbatim_3(martinique, annee = 2019)
#' verbatim3$encadre_paragraphe
creer_verbatim_3 <- function(indicateurs_rpls, annee) {

  data <- indicateurs_rpls
  
  #Datafields used 
  TypeZone <- millesime <- nb_mes <- nb_mes_qpv_construit_org <- 
    nb_mes_qpv_acq_vefa <- nb_mes_nonqpv_construit_org <- 
    nb_mes_nonqpv_acq_vefa <- nb_mes_qpv_acq_av_travaux <- 
    nb_mes_qpv_acq_ss_travaux <- nb_mes_nonqpv_acq_av_travaux <- 
    nb_mes_nonqpv_acq_ss_travaux <- CodeZone <- NULL
  
  # on calcule d'abord les indicateurs necessaires aux commentaires
  mise_en_serv <- data %>%
    dplyr::filter(grepl("gions", TypeZone)) %>%
    dplyr::arrange(desc(millesime)) %>%  dplyr::slice(1:5) %>%
    dplyr::mutate(rang = rank(nb_mes, na.last = TRUE, ties.method = "first")) %>%
    dplyr::mutate(class_mes = dplyr::case_when(
      rang == 1 ~  "du plus faible volume",
      rang == 2 ~  "du deuxi\u00e8me plus faible volume",
      rang == 3 ~  "du volume m\u00e9dian",
      rang == 4 ~  "du deuxi\u00e8me plus important volume",
      rang == 5 ~  "du plus important volume",
      TRUE ~ "")
    ) %>%
    dplyr::filter(millesime == annee) %>%
    dplyr::mutate(pourc_lgt_neuf = ((nb_mes_qpv_construit_org + nb_mes_qpv_acq_vefa + nb_mes_nonqpv_construit_org + nb_mes_nonqpv_acq_vefa) / nb_mes * 100) %>% format_fr_pct,
                  pourc_lgt_neuf_cons = ((nb_mes_qpv_construit_org + nb_mes_nonqpv_construit_org) / nb_mes * 100) %>% format_fr_pct ,
                  pourc_lgt_neuf_vefa = ((nb_mes_qpv_acq_vefa + nb_mes_nonqpv_acq_vefa) / nb_mes * 100) %>% format_fr_pct,
                  pourc_lgt_acquis = ((nb_mes_qpv_acq_av_travaux + nb_mes_qpv_acq_ss_travaux + nb_mes_nonqpv_acq_av_travaux + nb_mes_nonqpv_acq_ss_travaux) / nb_mes * 100) %>% format_fr_pct,
                  pourc_rehab = ((nb_mes_qpv_acq_av_travaux + nb_mes_nonqpv_acq_av_travaux) / (nb_mes_qpv_acq_av_travaux + nb_mes_qpv_acq_ss_travaux + nb_mes_nonqpv_acq_av_travaux + nb_mes_nonqpv_acq_ss_travaux) * 100) %>% format_fr_pct,
                  nb_mes_acq_ss_travaux = nb_mes_qpv_acq_ss_travaux + nb_mes_nonqpv_acq_ss_travaux,
                  nb_mes_acq_av_travaux = nb_mes_qpv_acq_av_travaux + nb_mes_nonqpv_acq_av_travaux)

  annee_prec <- data %>%
    dplyr::filter(grepl("gions", TypeZone), millesime == annee - 1)

  mes_acq <- dplyr::case_when((mise_en_serv$nb_mes_acq_ss_travaux + mise_en_serv$nb_mes_acq_av_travaux) == 0 ~ 0,
                              mise_en_serv$nb_mes_acq_av_travaux == 0 ~ 1,
                              TRUE ~ 2)


  # on récupère les formulations idiomatiques grâce à {propre.datareg}
  id_reg <- dplyr::filter(data, grepl("gions", TypeZone)) %>% dplyr::pull(CodeZone) %>% unique %>% as.character
  verb_reg <- propre.datareg::datareg(code_reg = id_reg)


  # on cree ensuite une liste nommee des differents parametres
  verb3 <- list(nom_reg = verb_reg$dans_la_region_nom_region,
                annee_prec = annee - 1,
                nb_mes = mise_en_serv$nb_mes %>%
                  format_fr_nb(dec = 0),
                class_mes = mise_en_serv$class_mes,
                pourc_lgt_neuf = mise_en_serv$pourc_lgt_neuf,
                pourc_lgt_neuf_cons = mise_en_serv$pourc_lgt_neuf_cons,
                pourc_lgt_neuf_vefa = mise_en_serv$pourc_lgt_neuf_vefa,
                pourc_lgt_acquis = mise_en_serv$pourc_lgt_acquis,
                pourc_rehab = mise_en_serv$pourc_rehab,
                nb_ls_actif_n = mise_en_serv$nb_ls_actif %>%
                  format_fr_nb(dec = 0),
                nb_ls_actif_n_1 = annee_prec$nb_ls_actif %>%
                  format_fr_nb(dec = 0),
                nb_demolition = mise_en_serv$nb_demolition %>%
                  format_fr_nb(dec = 0),
                nb_ventes = mise_en_serv$nb_ventes %>%
                  format_fr_nb(dec = 0),
                nb_sorties_autres_motifs= mise_en_serv$nb_sorties_autres_motifs %>%
                  format_fr_nb(dec = 0),
                nb_vente_bailleur = mise_en_serv$nb_vente_bailleur %>%
                  format_fr_nb(dec = 0))


  # production du verbatim a partir des elements precedents
  verbatim_chap_3 <- list(
    intertitre = "", 
    commentaires = "", 
    encadre_titre = "", 
    encadre_paragraphe= ""
  )
  
  
  verbatim_chap_3$intertitre <- glue::glue(
    "En {verb3$annee_prec}, {verb3$nb_mes} logements sociaux ont \u00e9t\u00e9 mis en service")
  
  verbatim_chap_3$commentaires <- glue::glue(
    paste(
      "Entre le 2 janvier {verb3$annee_prec} et le 1er janvier {annee},",
      "{verb3$nb_mes} logements sociaux ont \u00e9t\u00e9 mis en service {verb3$nom_reg}.",
      "Il s\u0027agit {verb3$class_mes} constat\u00e9 sur les cinq derni\u00e8res ann\u00e9es.",
      "Parmi ces mises en services, {verb3$pourc_lgt_neuf} sont des logements neufs,",
      "c\u0027est-\u00e0-dire construits par l\u0027organisme ({verb3$pourc_lgt_neuf_cons})",
      "ou acquis en vente en l\u0027\u00e9tat futur d\u0027ach\u00e8vement ({verb3$pourc_lgt_neuf_vefa})."
    )
  )
  
  if (mes_acq == 0) { 
    verbatim_chap_3$commentaires <- glue::glue(
      verbatim_chap_3$commentaires,
      paste(
        " En {verb3$annee_prec}, aucun bailleur n\u0027a mis en service de",
        "logements acquis dans le parc priv\u00e9."))
  } else { 
    verbatim_chap_3$commentaires <- glue::glue(
      verbatim_chap_3$commentaires,
      paste(
        " Les logements mis en service peuvent \u00eatre \u00e9galement",
        "des logements existants acquis en dehors du parc social.",
        "Les acquisitions dans le parc priv\u00e9 repr\u00e9sentent {verb3$pourc_lgt_acquis}",
        "des mises en service en {verb3$annee_prec}"))
  }

  if (mes_acq == 1) { verbatim_chap_3$commentaires <- glue::glue(verbatim_chap_3$commentaires, ".")}
  
  if (mes_acq == 2) { verbatim_chap_3$commentaires <- glue::glue(
    verbatim_chap_3$commentaires, 
    " ; {verb3$pourc_rehab} d\u0027entre elles se font avec des travaux de r\u00e9habilitation.")
  }

  verbatim_chap_3$encadre_titre <- glue::glue("Les mouvements du parc en {verb3$annee_prec}")
  
  verbatim_chap_3$encadre_paragraphe <- glue::glue(
    paste(
      "{propre.datareg::maj1let(verb3$nom_reg)}, le nombre de logements sociaux est de {verb3$nb_ls_actif_n}",
      "au 1er janvier {annee} contre {verb3$nb_ls_actif_n_1} au 1er janvier {verb3$annee_prec}.", 
      "Au cours de l\u0027ann\u00e9e {verb3$annee_prec}, {verb3$nb_mes} logements sociaux",
      "ont \u00e9t\u00e9 mis en service dans la r\u00e9gion. Dans le m\u00eame temps,",
      
      '{ifelse(verb3$nb_demolition>0,verb3$nb_demolition,"")}',
      '{ifelse(verb3$nb_demolition == 1," logement a \u00e9t\u00e9 d\u00e9moli, ","")}',
      '{ifelse(verb3$nb_demolition>1," logements ont \u00e9t\u00e9 d\u00e9molis, ","")}',
      
      '{ifelse(verb3$nb_ventes>0,verb3$nb_ventes,"")}',
      '{ifelse(verb3$nb_ventes == 1," logement a \u00e9t\u00e9 vendu","")}',
      '{ifelse(verb3$nb_ventes>1," logements ont \u00e9t\u00e9 vendus","")}',
      
      '{ifelse(verb3$nb_sorties_autres_motifs>0,", et ","")}',
      '{ifelse(verb3$nb_sorties_autres_motifs>0,verb3$nb_sorties_autres_motifs,"")}',
      '{ifelse(verb3$nb_sorties_autres_motifs == 1," logement a chang\u00e9 d\u0027usage ou a \u00e9t\u00e9 restructur\u00e9","")}',
      '{ifelse(verb3$nb_sorties_autres_motifs>1," logements ont chang\u00e9 d\u0027usage ou ont \u00e9t\u00e9 restructur\u00e9s","")}.',
      
      '{ifelse(verb3$nb_vente_bailleur>0,"Sans incidence sur le nombre de logements du parc social, ","")}',
      '{ifelse(verb3$nb_vente_bailleur>0,verb3$nb_vente_bailleur,"")}',
      '{ifelse(verb3$nb_vente_bailleur == 1," logement a fait l\u0027objet d\u0027un transfert entre bailleurs. ","")}',
      '{ifelse(verb3$nb_vente_bailleur>1," logements ont fait l\u0027objet d\u0027un transfert entre bailleurs. ","")}'
    )
  )


  verbatim_chap_3
}

