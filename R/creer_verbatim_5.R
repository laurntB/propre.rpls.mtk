#'
#' @encoding UTF-8
#' 
#' @title Chapitre 5 : Verbatim
#'
#' @description Production des commentaires verbatim du chapitre 5.
#'
#' @param indicateurs_rpls La table d'indicateurs préparée par
#'     dataprep() selon les inputs de l'utilisateur et filtrée sur le
#'     booléen Zone_ref
#' @param annee Le millésime renseigné par l'utilisateur (au format
#'     numérique)
#'
#' @return Un vecteur de 2 chaînes de caractères comprenant
#'     l'intertitre et les commentaires essentiels du chapitre 5
#'
#' @importFrom dplyr filter mutate select pull
#' @importFrom glue glue
#' @importFrom tidyr pivot_wider
#' @importFrom propre.datareg datareg maj1let
#'
#' @export
#'
#' @examples
#' creer_verbatim_5(martinique, annee = 2019)[["intertitre"]]
#' creer_verbatim_5(martinique, annee = 2019)[["commentaires"]]
creer_verbatim_5 <- function(indicateurs_rpls, annee) {

  data <- indicateurs_rpls
  
  #Datafields used 
  millesime <- TypeZone <- CodeZone <- nb_ls_loue <- nb_ls_vacant <- 
    nb_ls_loues_proploc <- nb_ls_vacant_3 <- num_mob <- denom_mob <- 
    taux_vac <- taux_vac_3 <- taux_mob <- Zone <- NULL
  
  # Création de la table avec tous les indicateurs calculés
  vac_mob <- data %>%
    # Filtre pour ne conserver que les données de l'année choisie, de la région choisie et de la maille nationale (FRMETRO ou FRMETRODROM)
    dplyr::filter(millesime == annee, grepl("gions", TypeZone) | grepl("FRMETRO", CodeZone)) %>%
    # Création de variables utiles, notamment indicateurs calculés
    dplyr::mutate(TypeZone = substr(TypeZone, 1, 1),
                  nb_ls_loues_proploc = nb_ls_loue + nb_ls_vacant, # lgts proposés à la location
                  taux_vac = nb_ls_vacant / nb_ls_loues_proploc * 100,
                  taux_vac_3 = nb_ls_vacant_3 / nb_ls_loues_proploc * 100,
                  taux_mob = num_mob / denom_mob * 100) %>%
    # sélection des variables utiles
    dplyr::select(TypeZone, nb_ls_loues_proploc, nb_ls_vacant, taux_vac, nb_ls_vacant_3, taux_vac_3, taux_mob) %>%
    # Passage du format long au format large
    tidyr::pivot_wider(names_from = TypeZone, values_from = c(nb_ls_loues_proploc,
                                                                    nb_ls_vacant, taux_vac,
                                                                    nb_ls_vacant_3, taux_vac_3,
                                                                    taux_mob)) %>%
    # Création des variables relatives aux noms des mailles régionale (nom région) et nationale (FRMETRO ou FRMETRODROM)
    dplyr::mutate(region = data %>%
                    dplyr::filter(grepl("gions", TypeZone)) %>%
                    dplyr::pull(Zone) %>% unique() %>% as.character(),
                  nomzone = ifelse(data %>%
                                     dplyr::filter(grepl("FRMETRO", CodeZone)) %>%
                                     dplyr::pull(CodeZone)%>% unique() %>% as.character() == "FRMETRO",
                                   "en France m\u00e9tropolitaine", "sur l\u2019ensemble de la France"))


  # Récupération des formulations idiomatiques grâce à {propre.datareg}
  id_reg <- dplyr::filter(data, grepl("gions", TypeZone)) %>% dplyr::pull(CodeZone) %>% unique %>% as.character
  verb_reg <- propre.datareg::datareg(code_reg = id_reg)


  # Création de liste avec tous les paramètres utiles au commentaire
  verb5 <- list(nb_vac_reg = vac_mob$nb_ls_vacant_R %>% format_fr_nb(dec = 0),
                nb_vac_3_reg = vac_mob$nb_ls_vacant_3_R %>% format_fr_nb(dec = 0),
                annee = annee,
                nb_ls_loues_proploc_reg = vac_mob$nb_ls_loues_proploc_R %>% format_fr_nb(dec = 0),
                region = verb_reg$dans_la_region_nom_region,
                taux_vac_reg = vac_mob$taux_vac_R %>% format_fr_pct,
                taux_vac_fr = vac_mob$taux_vac_F %>% format_fr_pct,
                france = vac_mob$nomzone,
                taux_vac_3_reg = vac_mob$taux_vac_3_R %>% format_fr_pct,
                taux_vac_3_fr = vac_mob$taux_vac_3_F %>% format_fr_pct,
                annee_moins1 = annee - 1,
                taux_mob_reg = vac_mob$taux_mob_R %>% format_fr_pct,
                taux_mob_fr = vac_mob$taux_mob_F %>% format_fr_pct,
                loc_intertitre = verb_reg$dans_la_region)


  # Production du verbatim
  verbatim_chap_5 <- list(intertitre ="", commentaires ="")

  verbatim_chap_5$intertitre <- if (verb5$nb_vac_reg == "0") {
    glue::glue("Au 1er janvier ", {verb5$annee}, ", aucun logement social n'est vacant {verb5$loc_intertitre}.")
  } else {
    glue::glue("{propre.datareg::maj1let(verb5$loc_intertitre)}, {verb5$nb_vac_reg} logements sont vacants et {verb5$nb_vac_3_reg} le sont depuis plus de trois mois.")
  }

  verbatim_chap_5$commentaires <- if (verb5$nb_vac_reg == "0") {
    glue::glue("Au 1er janvier {verb5$annee}, parmi les {verb5$nb_ls_loues_proploc_reg} logements lou\u00e9s ou propos\u00e9s \u00e0 la location {verb5$region}, aucun n'est vacant, contre {verb5$taux_vac_fr} {verb5$france}.
               {propre.datareg::maj1let(verb5$region)}, en {verb5$annee_moins1}, {verb5$taux_mob_reg} de logements ont chang\u00e9 de locataire, contre {verb5$taux_mob_fr} {verb5$france}.")

  } else {
    glue::glue("Au 1er janvier {verb5$annee}, parmi les {verb5$nb_ls_loues_proploc_reg} logements lou\u00e9s ou propos\u00e9s \u00e0 la location {verb5$region}, {verb5$taux_vac_reg} sont vacants, contre {verb5$taux_vac_fr} {verb5$france}. La vacance de plus de trois mois, dite \u00ab vacance structurelle \u00bb est de {verb5$taux_vac_3_reg}, contre {verb5$taux_vac_3_fr} au niveau national.
               {propre.datareg::maj1let(verb5$region)}, en {verb5$annee_moins1}, {verb5$taux_mob_reg} de logements ont chang\u00e9 de locataire, contre {verb5$taux_mob_fr} {verb5$france}.")

  }

  # Renvoi du résultat
  verbatim_chap_5

}
