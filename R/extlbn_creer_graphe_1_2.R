#' Description
#'
#' Détails
#'
#' @encoding UTF-8
#' 
#' @title extlbn_creer_graphe_1_2
#' @param indicateurs_rpls indicateurs_rpls
#' @param annee annee
#' @return graphique
#' @author LBn
#' @export
#' @include filtres_communs.R
#' @importFrom dplyr select transmute
#' @importFrom ggplot2 theme theme_minimal element_blank element_text ggplot geom_bar coord_polar geom_text aes position_stack labs scale_y_continuous 
#' @importFrom glue glue
#' @importFrom gouvdown scale_fill_gouv_discrete
#' @importFrom ggiraph ggiraph
extlbn_creer_graphe_1_2 <- function(indicateurs_rpls,annee)
{
  
  nom_region <- region_cible(indicateurs_rpls)
  id_region <- get_id_reg(nom_region)
    
  palette <- "pal_gouv_qual2"
  
  epci_annee_n <- selectionner_donnees_epci(indicateurs_rpls, nom_region, annee)
  
  #Datafields used 
  Zone <- nb_logt_total <- Percent <- theme_minimal <- element_text <- 
    lblpct <- NULL
      
  data <- epci_annee_n %>%
    select(Zone, nb_logt_total) %>%
    transmute( 
      Zone, 
      Percent = nb_logt_total / sum(nb_logt_total) * 100,
      lblpct = Percent %>% format_fr_pct()
    )
  
  pie_theme <- theme_minimal()+
    theme(
      axis.title.x = element_blank(),
      axis.title.y = element_blank(),
      panel.border = element_blank(),
      panel.grid=element_blank(),
      axis.ticks = element_blank(),
      axis.text.x=element_blank(),
      plot.title=element_text(size=14, face="bold"),
      # legend.position = "bottom"
    )
  
  graphe <- ggplot2::ggplot(data, aes(x="", y=Percent, fill=Zone, label=lblpct)) +
    ggplot2::geom_bar(width=1, stat="identity") +
    ggplot2::coord_polar("y", start=0) +
    ggplot2::theme(axis.text.x=element_blank()) +
    ggplot2::geom_text(
      ggplot2::aes(x = 1.8),
      position = ggplot2::position_stack(vjust=0.5)) +
    ggplot2::labs(title = glue::glue("Parc locatif des bailleurs sociaux au 01/01/{annee}"),
                  subtitle = "R\u00e9partition en % par EPCI",
                  x = "",
                  y = "",
                  caption = caption(sources = 1)) +
    pie_theme +
    gouvdown::scale_fill_gouv_discrete(palette = palette) +
    ggplot2::scale_y_continuous( labels = format_fr_nb )
  
  ggiraph::ggiraph(code = print(graphe))
}
