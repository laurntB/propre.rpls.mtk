prepdonnees_tableau_6_1 <- function(indicateurs_rpls,annee)
{
    nom_region <- region_cible(indicateurs_rpls)
    
    ## Datafields used 
    TypeZone <- Zone <- millesime <- loyer_m2 <- loyer_m2_60ans_et_plus <- 
        loyer_m2_40_59ans <- loyer_m2_20_39ans <- loyer_m2_10_19ans <- 
            loyer_m2_5_9ans <- loyer_m2_0_4ans <- NULL
    
    loymoy <- indicateurs_rpls %>%
        select(TypeZone, Zone, millesime, loyer_m2, loyer_m2_60ans_et_plus,
               loyer_m2_40_59ans,loyer_m2_20_39ans,loyer_m2_10_19ans,
               loyer_m2_5_9ans,loyer_m2_0_4ans) %>%
        filter(TypeZone=='R\u00E9gions' & Zone == nom_region)
    
    tableau <- loymoy %>%
        dplyr::select(millesime, loyer_m2)
}











#' desc
#'
#' details
#' 
#' @encoding UTF-8
#' @title extlbn_creer_tableau_6_1
#' @param indicateurs_rpls indicateurs_rpls
#' @param annee annee 
#' @return kableExtra
#' @author LBn
#' @export 
#' @include filtres_communs.R
#' @importFrom dplyr filter select
#' @importFrom kableExtra kable footnote
extlbn_creer_tableau_6_1 <- function(indicateurs_rpls,annee)
{
    
    data <- prepdonnees_tableau_6_1(indicateurs_rpls,annee)

    gntd_caption <- mktbl_caption(
        paste0("Evolution du loyer moyen dans le temps"),
        "t61")
    
    data%>%
        knitr::kable(
                   "html",
                   col.names=c("Ann\u00E9e",
                               "Loyer moyen"),
                   escape = FALSE,
                   digits  = , c(1,1),
                   format.args = list(big.mark = " ",
                                      decimal.mark = ","),
                   caption = gntd_caption,
                   label = NA,
                   booktabs = TRUE)%>%
        kableExtra::kable_styling(font_size = 12) %>%
                                        # Création note de bas de page
        kableExtra::footnote(general = caption(sources = 1), general_title = "")
    
    
}
