


#' Description
#'
#' Détails
#' 
#' @encoding UTF-8
#' @title calculer_indicateurs_2_1
#' @param indicateurs_rpls indicateurs_rpls 
#' @param annee annee
#' @importFrom dplyr filter pull
#' @return list
#' @author LBN
#' @export
calculer_indicateurs_2_1 <- function(indicateurs_rpls, annee)
{
  nom_region <- region_cible(indicateurs_rpls)
  
  #Datafields used 
  millesime <- CodeZone <- nb_ls_coll <- nb_ls_ind <- nb_logt_total <- 
    Zone <- NULL
  
  indicateurs_france <- indicateurs_rpls   %>% 
    filter(millesime==annee & CodeZone == 'FRMETRODROM')
  
  f_nb_coll          <- indicateurs_france %>% pull(nb_ls_coll)
  f_nb_ind           <- indicateurs_france %>% pull(nb_ls_ind)
  f_nb_tot           <- indicateurs_france %>% pull(nb_logt_total)
  f_pct_coll         <- f_nb_coll * 100 / f_nb_tot
  f_pct_ind          <- f_nb_ind * 100 / f_nb_tot
  
  indicateurs_region <- indicateurs_rpls   %>% 
    filter(millesime==annee & Zone == nom_region)
  r_nb_coll          <- indicateurs_region %>% pull(nb_ls_coll)
  r_nb_ind           <- indicateurs_region %>% pull(nb_ls_ind)
  r_nb_tot           <- indicateurs_region %>% pull(nb_logt_total)
  r_pct_coll         <- r_nb_coll * 100 / r_nb_tot
  r_pct_ind          <- r_nb_ind * 100 / r_nb_tot
  
  list(
    "indicateurs_france"=indicateurs_france[[1]],
    "f_nb_coll"=f_nb_coll[[1]],
    "f_nb_ind"=f_nb_ind[[1]],
    "f_nb_tot"=f_nb_tot[[1]],
    "f_pct_coll"=f_pct_coll[[1]],
    "f_pct_ind"=f_pct_ind[[1]],
    "indicateurs_region"=indicateurs_region[[1]],
    "r_nb_coll"=r_nb_coll[[1]],
    "r_nb_ind"=r_nb_ind[[1]],
    "r_nb_tot"=r_nb_tot[[1]],
    "r_pct_coll"=r_pct_coll[[1]],
    "r_pct_ind"=r_pct_ind[[1]]
  )
}
