#' Mentions legales : Verbatim
#'
#' @encoding UTF-8
#' 
#' @description Production des commentaires verbatim des mentions légales.
#'
#' @param date_publication La date de publication. Par défaut la date
#'     du jour, pour personnaliser, utiliser le format JJ/MM/AAAA.
#' @param nom_region Le nom de la region au format texte, tel qu'issu
#'     de la saisie utilisateur, par exemple '01 Guadeloupe'.
#'
#' @return Un vecteur de 11 chaînes de caractères comprenant la date
#'     de publication, les informations relatives au service
#'     gestionnaire, à la direction de publication, au développement,
#'     à l'issn, à l'hébergement, aux droits d'auteur, aux codes
#'     sources, à la création de liens, aux usages et au traitements
#'     des données personnelles.
#'
#' @importFrom dplyr filter slice pull
#' @importFrom glue glue
#' @importFrom lubridate ymd today
#' @importFrom propre.datareg datareg
#' @importFrom stringr str_sub
#'
#'
#'
#' @export
#'
#' @examples
#' creer_verbatim_mentions_legales(nom_region = "Bretagne")
creer_verbatim_mentions_legales <- function(date_publication = lubridate::today(), nom_region = "Bretagne") {

  # Formatage de la date
  if(date_publication == lubridate::today()){

    date_publication <- format(lubridate::ymd(date_publication), format = "%d %B %Y")
  } else {
    #On part du principe que l utilisateur remplira une date sous format jj/mm/aaaa
    date_publication <- format(lubridate::ymd(paste0(stringr::str_sub(date_publication, 7, 10),
                                                     stringr::str_sub(date_publication, 4, 5),
                                                     stringr::str_sub(date_publication, 1, 2))), format = "%d %B %Y")
  }

  # Récupération du code région et des donnees regionales de mentions legales
  id_reg <- get_id_reg(nom_region)
  mentions_reg <- propre.datareg::datareg(code_reg = id_reg)

  # Création de la liste avec les parties des mentions légales contenant des éléments à paramétrer
  verbatim_mentions_legales <- list(date = "", service_gestionnaire = "", direction_publication = "", developpement ="",
                                    issn = "", hebergement = "", droit_auteur = "", code_source = "", lien = "",
                                    usage = "", donnees_perso = "")

  # Paramétrage de la date de publication
  verbatim_mentions_legales$date <- glue::glue("Publi\u00e9 le {date_publication}")

  # Paramétrage des infos relatives au service gestionnaire
  verbatim_mentions_legales$service_gestionnaire <- glue::glue(
"{mentions_reg$nom_dreal}\n
{mentions_reg$adresse}\n
T\u00e9l\u00e9phone : {mentions_reg$telephone}\n
Courriel : {mentions_reg$courriel_contact}")

  # Paramétrage des infos relatives à la direction de publication
  verbatim_mentions_legales$direction_publication <-  glue::glue(
      paste0(
          "{mentions_reg$prenom_directeur_directrice} ",
          "{mentions_reg$nom_directeur_directrice}, ",
          "{mentions_reg$titre}."))

  # Paramétrage des infos relatives au développement
  verbatim_mentions_legales$developpement <- paste0(c(
      "  - Laurent Beltran (d\u00E9clinaison dom)",
      "  - Cl\u00e9ment Belliard",
      "  - Fabio Dos Santos Pereira",
      "  - Juliette Engelaere-Lefebvre",
      "  - Franck Gaspard",
      "   -Daniel Kalioudjoglou",
      "  - Murielle Lethrosne",
      "  - Jean-Bernard Salomond",
      "  - Mael Theuli\u00e8re",
      "  - Arnaud Wilczynski",
      "  - Marouane Zellou"), collapse ="\n")

  # Paramétrage des infos relatives à l'ISSN
  verbatim_mentions_legales$issn <- "En cours"

  # Paramétrage des infos relatives à l'hébergement
  verbatim_mentions_legales$hebergement <- "http://dreal.statistiques.developpement-durable.gouv.fr/"

  # Paramétrage du texte sur le droit d'auteur
    verbatim_mentions_legales$droit_auteur <- glue::glue(paste0(
"Tous les contenus pr\u00e9sents sur ce site sont couverts par le droit d\'auteur. Toute reprise
est d\u00e8s lors conditionn\u00e9e \u00e0 l\'accord de l\'auteur en vertu de l\'article L.122-4
du Code de la Propri\u00e9t\u00e9 Intellectuelle.\n Toutes les informations li\u00e9es \u00e0
cette publication (donn\u00e9es et textes) sont publi\u00e9es sous licence ouverte/open
licence v2 (dite licence Etalab) : quiconque est libre de r\u00e9utiliser ces informations
sous r\u00e9serve, notamment, d\'en mentionner la filiation.\n Tous les scripts sources du package
sont disponibles sous licence [GPL-3.0-or-later](https://spdx.org/licenses/GPL-3.0-or-later.html).
\n La
[charte graphique de la marque d'\u00c9tat](https://www.gouvernement.fr/charte/charte-graphique-les-fondamentaux/la-typographie)
est \u00e0 usage exclusif des acteurs de la sphe\u0300re e\u0301tatique.  En particulier,
la typographie Marianne\u00a9 est prot\u00e9g\u00e9e par le droit d'auteur.
"))

  # Paramétrage des infos relatives aux codes sources
  verbatim_mentions_legales$code_source <- 
"L\u2019ensemble des scripts de collecte et de datavisualisation est disponible sur le r\u00e9pertoire
gitlab du r\u00e9seau des statisticiens en DREAL (https://gitlab.com/rdes_dreal/propre.rpls).
Vous pouvez y reporter les \u00e9ventuels bugs ou demandes d'\u00e9volution au niveau de la rubrique _Issues_."

  # Paramétrage des infos relatives aux liens
  verbatim_mentions_legales$lien <- 
"Tout site public ou priv\u00e9 est autoris\u00e9 \u00e0 \u00e9tablir, sans autorisation pr\u00e9alable,
un lien vers les informations diffus\u00e9es par le Minist\u00e8re de la Transition \u00c9cologique
et le Minist\u00e8re de la Coh\u00e9sion des Territoires et des Relations avec les Collectivit\u00e9s
Territoriales.\n
L\u2019autorisation de mise en place d\'un lien est valable pour tout support, \u00e0 l\'exception
de ceux diffusant des informations \u00e0 caract\u00e8re pol\u00e9mique, pornographique, x\u00e9nophobe
ou pouvant, dans une plus large mesure porter atteinte \u00e0 la sensibilit\u00e9 du plus grand nombre.\n
Pour ce faire, et toujours dans le respect des droits de leurs auteurs, une ic\u00f4ne Marianne est
disponible sur le site https://www.gouvernement.fr/marque-Etat pour agr\u00e9menter votre lien et
pr\u00e9ciser que le site d\'origine est celui du Minist\u00e8re de la Transition \u00c9cologique
ou du Minist\u00e8re de la Coh\u00e9sion des Territoires et des Relations avec les
Collectivit\u00e9s Territoriales."

  # Paramétrage du texte sur les usages
  verbatim_mentions_legales$usage <- glue::glue(
"Les utilisateurs sont responsables des interrogations qu\'ils formulent ainsi que de
l\'interpr\u00e9tation et de l\'utilisation qu\'ils font des r\u00e9sultats.
Il leur appartient d\'en faire un usage conforme aux r\u00e9glementations en vigueur
et aux recommandations de la CNIL lorsque des donn\u00e9es ont un caract\u00e8re nominatif
(loi n\u00b0 78.17 du 6 janvier 1978, relative \u00e0 l\'informatique, aux fichiers et
aux libert\u00e9s dite loi informatique et libert\u00e9s).\n
Il appartient \u00e0 l\'utilisateur de ce site de prendre toutes les mesures appropri\u00e9es
de fa\u00e7on \u00e0 prot\u00e9ger ses propres donn\u00e9es et/ou logiciels de la contamination
par d\'\u00e9ventuels virus circulant sur le r\u00e9seau Internet.
De mani\u00e8re g\u00e9n\u00e9rale, la {mentions_reg$nom_dreal} d\u00e9cline toute
responsabilit\u00e9 quant \u00e0 un \u00e9ventuel dommage survenu pendant la consultation
du pr\u00e9sent site.
Les messages que vous pouvez nous adresser transitant par un r\u00e9seau ouvert de
t\u00e9l\u00e9communications, nous ne pouvons assurer leur confidentialit\u00e9.")

  # Paramétrage des infos relatives au traitement des données personnelles
  verbatim_mentions_legales$donnees_perso <-
      "L\u2019outil ne fait pas d\'usage interne de donn\u00e9es \u00e0 caract\u00e8re personnel."

  # Affichage du résultat
  verbatim_mentions_legales

}
