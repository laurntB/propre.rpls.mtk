#' Creation de deux graphiques du chapitre anciennete et etat
#' energetique, represantant la repartition des logements selon la
#' classe DPE energie (A a G) et selon la classe DPE effet de serre (A
#' a G).
#'
#' @encoding UTF-8
#' 
#' @description Création de deux diagrammes en barres représentant le
#'     pourcentage de logements selon les étiquettes DPE (de A à G)
#'     pour la consommation d'énergie et les émissions de gaz à effet
#'     de serre à l'échelle régionale.
#'
#' @param data La table d'indicateurs préparée par dataprep() selon
#'     les inputs de l'utilisateur et filtrée sur le booléen Zone_ref.
#' @param annee Une année parmi les millésimes sélectionnables par
#'     l'utilisateur, au format numérique.
#' @param titre_nrj une chaine de caractère si vous voulez ajouter un
#'     titre spécifique au graphe sur le dpe energie. (par défaut:
#'     "Répartition des logements selon leur classe de consommation d'énergie au 1er janvier {annee}")
#' @param titre_effet_serre une chaine de caractère si vous voulez
#'     ajouter un titre spécifique au graphe sur le dpe CO2 (par
#'     défaut:
#'     "Répartition des logements selon leur classe d'impact des consommations d'énergie sur l'effet de serre au 1er janvier {annee}")
#' @param note_de_lecture_nrj une chaine de caractère si voulez faire
#'     une note de lecture au 1er graph sur le dpe energie
#' @param note_de_lecture_effet_serre une chaine de caractère si
#'     voulez faire une note de lecture au 2e graph sur le dpe CO2
#'
#' @return une composition de deux graphiques en barres interactifs
#'     pour les régions métropolitaines, rien sinon
#'
#' @importFrom dplyr filter select mutate pull group_by
#' @importFrom ggiraph geom_bar_interactive ggiraph
#' @importFrom ggplot2 ggplot aes position_dodge labs guides guide_legend theme scale_fill_discrete scale_fill_manual coord_flip geom_text element_blank scale_y_continuous
#' @importFrom gouvdown scale_fill_gouv_discrete
#' @importFrom stringr str_wrap
#' @importFrom tidyr pivot_longer
#' @importFrom rlang .data
#' @importFrom forcats fct_relevel fct_rev
#' @importFrom scales percent
#' @importFrom patchwork plot_layout
#' @importFrom  glue glue
#' @export
#'
#' @examples
#' creer_graphe_4_1(martinique, annee = 2019,
#' note_de_lecture_nrj = "",
#' note_de_lecture_effet_serre = "")



creer_graphe_4_1 <- function(data, annee,
                             titre_nrj = NULL,
                             titre_effet_serre = NULL,
                             note_de_lecture_nrj = "",
                             note_de_lecture_effet_serre = ""){
  
  
  #Datafields used 
  CodeZone <- TypeZone <- millesime <- Zone <- nb_ls_dpe_ener_A <- 
    nb_ls_dpe_ener_B <- nb_ls_dpe_ener_C <- nb_ls_dpe_ener_D <- 
    nb_ls_dpe_ener_E <- nb_ls_dpe_ener_F <- nb_ls_dpe_ener_G <- 
    nb_ls_dpe_serre_A <- nb_ls_dpe_serre_B <- nb_ls_dpe_serre_C <- 
    nb_ls_dpe_serre_D <- nb_ls_dpe_serre_E <- nb_ls_dpe_serre_F <- 
    nb_ls_dpe_serre_G <- Type_dpe <- Nb_logements <- 
    Percent_logements <- Classe_dpe <- NULL

  if ( is.null(titre_nrj)){
    titre_nrj <- "R\u00e9partition des logements selon leur classe de consommation d'\u00e9nergie au 1er janvier {annee}"}
  if ( is.null(titre_effet_serre)){
    titre_effet_serre <- "R\u00e9partition des logements selon leur classe d'impact des consommations d'\u00e9nergie sur l'effet de serre au 1er janvier {annee}"}
  # un booleen pour l'appartenance de la région à la France métropolitaine
  metro <- dplyr::filter(data, grepl("FRMETRO", CodeZone)) %>%
    dplyr::pull(CodeZone) %>% unique() %>%
    as.character() == "FRMETRO"

  # la fonction ne renvoie rien si la région choisie est DROM
  if(metro) {
    # Création du dataset utile à la production du graphique
    tab <- data %>%
      # Filtre pour ne garder que les données de la région choisie et du millésime sélectionné
      dplyr::filter(TypeZone == "R\u00e9gions",
                    millesime == annee) %>%
      # Sélection des variables utiles pour le graphique
      dplyr::select(TypeZone, Zone, millesime,
                    nb_ls_dpe_ener_A, nb_ls_dpe_ener_B, nb_ls_dpe_ener_C, nb_ls_dpe_ener_D,
                    nb_ls_dpe_ener_E, nb_ls_dpe_ener_F, nb_ls_dpe_ener_G,
                    nb_ls_dpe_serre_A, nb_ls_dpe_serre_B, nb_ls_dpe_serre_C, nb_ls_dpe_serre_D,
                    nb_ls_dpe_serre_E, nb_ls_dpe_serre_F, nb_ls_dpe_serre_G) %>%
      # Passage du format large au format long
      tidyr::pivot_longer(-c(TypeZone, Zone, millesime),
                          values_to = "Nb_logements", names_sep = "_", names_to = c("Type_dpe", "Classe_dpe"),
                          names_prefix = "nb_ls_dpe_") %>%
      # Modification des modalités de la variable Type_dpe
      dplyr::mutate(Type_dpe = ifelse(Type_dpe == "ener", "Classe \u00c9nergie", "Classe Effet de serre"),
                    # Modification de l'ordre des levels
                    Type_dpe = forcats::fct_relevel(Type_dpe, "Classe \u00c9nergie", "Classe Effet de serre")) %>%
      dplyr::group_by(Type_dpe) %>%
      dplyr::mutate(Percent_logements = 100 * prop.table(Nb_logements))


    ymax = max(dplyr::pull(tab,Percent_logements)) + 30

    # Création du graphique ENERGIE
    tab1 <- tab %>%
      dplyr::filter(Type_dpe == "Classe \u00c9nergie")

    graphe1 <- ggplot2::ggplot(data = tab1, mapping = ggplot2::aes(x = forcats::fct_rev(factor(Classe_dpe)), y = Percent_logements, fill = Classe_dpe)) +
      # Création graphique en barres en mode interactif
      ggiraph::geom_bar_interactive(stat = "identity", data_id = row.names(tab1),
                                    tooltip = dplyr::pull(tab1, Percent_logements) %>% format_fr_pct(),
                                    position = ggplot2::position_dodge()) +
      ggplot2::coord_flip() +
      ggplot2::geom_label(ggplot2::aes(label = format_fr_pct(Percent_logements)),
                          vjust = 0.3, hjust = -0.2 , size = 3.5, label.size = 0, fill = "white") +
      ggplot2::geom_label(ggplot2::aes(x = forcats::fct_rev(factor(Classe_dpe)), y = ymax), hjust = "right", label.size = 0, fill = "white",
                          label = c("\u2264 50 kWh/m\u00b2 par an","51 \u00e0 90","91 \u00e0 150","151 \u00e0 230","231 \u00e0 330","331 \u00e0 450","> 450")) +
      # Paramétrage titre, sous-titre, axes ...
      ggplot2::labs(title = stringr::str_wrap(glue::glue(titre_nrj), width = 50),
                    subtitle = "Unit\u00e9 : %",
                    x = "",
                    y = "",
                    caption = dplyr::if_else(note_de_lecture_nrj != "" ,
                                             paste0(note_de_lecture_nrj, "\n",
                                                    "Champ : logements ayant un DPE renseign\u00e9\n",
                                                    caption(sources = 1)),
                                             paste0("Champ : logements ayant un DPE renseign\u00e9\n\n",
                                                    caption(sources = 1)))) +
      # Retrait du titre de la légende
      ggplot2::guides(fill = ggplot2::guide_legend(title = "")) +
      # Positionnement de la légende sous le graphique
      ggplot2::theme(legend.position = "none",
                     axis.text.x = ggplot2::element_blank(),
                     axis.ticks.x = ggplot2::element_blank()) +
      ggplot2::scale_y_continuous(lim = c(0,ymax))+
      ggplot2::scale_fill_manual(breaks = c("A", "B", "C", "D", "E", "F", "G"),
                                 values = c("#319A31","#33CC33","#CCFF33","#FFFF00","#FFCC00","#FF9A33","#ff0000"))

    # Création du graphique GES
    tab2 <- tab %>%
      dplyr::filter(Type_dpe == "Classe Effet de serre")

    graphe2 <- ggplot2::ggplot(data = tab2, mapping = ggplot2::aes(x = forcats::fct_rev(factor(Classe_dpe)), y = Percent_logements, fill = Classe_dpe)) +
      # Création graphique en barres en mode interactif
      ggiraph::geom_bar_interactive(stat = "identity", data_id = row.names(tab2),
                                    tooltip = dplyr::pull(tab2, Percent_logements) %>% format_fr_pct(),
                                    position = ggplot2::position_dodge()) +
      ggplot2::coord_flip()+
      ggplot2::geom_label(ggplot2::aes(label = format_fr_pct(Percent_logements)),
                          vjust = 0.3, hjust = -0.2 , size = 3.5, label.size = 0, fill = "white") +
      ggplot2::geom_label(ggplot2::aes(x = forcats::fct_rev(factor(Classe_dpe)), y = ymax), hjust = "right", label.size = 0, fill = "white",
                          label = c("\u2264 5 kg eq CO2/m\u00b2 par an","6 \u00e0 10","11 \u00e0 20","21 \u00e0 35","36 \u00e0 55","56 \u00e0 80","> 80"))+
      # Paramétrage titre, sous-titre, axes ...
      ggplot2::labs(title = stringr::str_wrap(glue::glue(titre_effet_serre), width = 50),
                    subtitle = "Unit\u00e9 : %",
                    x = "",
                    y = "",
                    caption = dplyr::if_else(note_de_lecture_effet_serre != "" ,
                                             paste0(note_de_lecture_effet_serre, "\n",
                                                    "Champ : logements ayant un DPE renseign\u00e9\n",
                                                    caption(sources = 1)),
                                             paste0("Champ : logements ayant un DPE renseign\u00e9\n\n",
                                                    caption(sources = 1)))) +
      # Retrait du titre de la légende
      ggplot2::guides(fill = ggplot2::guide_legend(title = "")) +
      # Positionnement de la légende sous le graphique
      ggplot2::theme(legend.position = "none",
                     axis.text.x = ggplot2::element_blank(),
                     axis.ticks.x = ggplot2::element_blank())  +
      ggplot2::scale_y_continuous(lim = c(0, ymax))+
      ggplot2::scale_fill_manual(breaks = c("A", "B", "C", "D", "E", "F", "G"),
                                 values = c("#F6EDFD","#E1C2F8","#D4A9F5","#CB95F3","#BA72EF","#A74DEB","#8A19DF"))

    #Patchwork des graphiques
    g <- graphe1 + graphe2 + patchwork::plot_layout(ncol = 1)

    # Ajout de l'interactivité
    ggiraph::ggiraph(ggobj = g, width_svg = 6, height_svg = 10)
  }
}
